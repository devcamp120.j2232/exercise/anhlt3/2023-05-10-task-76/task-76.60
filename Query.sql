-- Viết query cho bảng customers cho phép tìm danh sách theo họ hoặc tên với LIKE
SELECT * FROM customers
WHERE last_name LIKE '%{lastName}%' OR first_name LIKE '%{firstName}%';

-- Viết query cho bảng customers cho phép tìm danh sách theo city, state với LIKE có phân trang
SELECT * FROM customers
WHERE city LIKE '%{city}%' AND state LIKE '%{state}%'
LIMIT {start}, {limit};

-- Viết query cho bảng customers cho phép tìm danh sách theo country có phân trang và ORDER BY tên
SELECT * FROM customers
WHERE country LIKE '%{country}%'
ORDER BY last_name ASC, first_name ASC
LIMIT {start}, {limit};

-- Viết query cho bảng customers cho phép UPDATE dữ liệu có country = NULL với giá trị truyền vào từ tham số
UPDATE customers
SET country = NULL
WHERE country = '{country}';

-- Làm tương tự cho payments , orders, order_details, products, product_lines, employees, offices
-- Tìm danh sách thanh toán theo mã khách hàng (customer_id) với phân trang
SELECT * FROM payments
WHERE customer_id = {customerId}
LIMIT {start}, {limit};

-- Tìm danh sách thanh toán theo ngày thanh toán (payment_date) với phân trang và sắp xếp
SELECT * FROM payments
WHERE payment_date LIKE '{paymentDate}%'
ORDER BY payment_date ASC
LIMIT {start}, {limit};

-- Truy vấn danh sách đơn hàng (Orders) theo mã khách hàng (customer_id) với phân trang
SELECT * FROM orders
WHERE customer_id = {customerId}
LIMIT {start}, {limit};

-- Truy vấn danh sách chi tiết đơn hàng (OrderDetail) theo mã đơn hàng (order_id) với phân trang
SELECT * FROM order_details
WHERE order_id = {orderId}
LIMIT {start}, {limit};

-- Truy vấn danh sách sản phẩm (Products) theo mã dòng sản phẩm (product_line_id) với phân trang
SELECT * FROM products
WHERE product_line_id = {productLineId}
LIMIT {start}, {limit};

-- Truy vấn danh sách dòng sản phẩm (ProductLine) với phân trang
SELECT * FROM product_lines
LIMIT {start}, {limit};

-- Truy vấn danh sách nhân viên (Employees) theo mã văn phòng (office_code) với phân trang
SELECT * FROM employees
WHERE office_code = {officeCode}
LIMIT {start}, {limit};

-- Truy vấn danh sách văn phòng (Offices) với phân trang
SELECT * FROM offices
LIMIT {start}, {limit};

-- Query tìm danh sách theo với LIKE và phân trang
SELECT * FROM payments WHERE customer_id = {customer_id};
SELECT * FROM orders WHERE status LIKE '%{status}%' LIMIT {offset}, {limit};
SELECT * FROM order_details WHERE order_id = {order_id} AND product_id = {product_id};
SELECT * FROM products WHERE product_code LIKE '%{code}%' OR product_name LIKE '%{name}%';
SELECT * FROM product_lines WHERE product_line LIKE '%{product_line}%' LIMIT {offset}, {limit};
SELECT * FROM employees WHERE email LIKE '%{email}%';
SELECT * FROM offices WHERE city LIKE '%{city}%' AND state LIKE '%{state}%' LIMIT {offset}, {limit};

